S3 Client
=========

Features::

  - Autoset key name with filename.
  - Can set Content-Type header.
  - Show all buckets.
  - Show all keys.

This script has been tested in kilatstorage.com.

Based on Kilatstorage_using_Python_.

.. _Kilatstorage_using_Python: http://kb.cloudkilat.com/manajemen-kilat-storage-python
