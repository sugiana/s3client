# http://kb.cloudkilat.com/manajemen-kilat-storage-python/

import sys
import os
import imp
from optparse import OptionParser
import boto.s3.connection


def get_bucket(create=False):
    if not option.bucket:
        error('Need --bucket option.')
    bucket = conn.lookup(option.bucket)
    if bucket:
        return bucket
    if create:
        print('Create bucket {b}'.format(b=option.bucket))
        return conn.create_bucket(option.bucket)
    error('Bucket {b} not found.'.format(b=option.bucket))

def get_key():
    key = bucket.lookup(option.key)
    if key:
        return key
    error('Key {k} not found on {b} bucket.'.format(k=option.key,
            b=bucket.name))

def error(s):
    print(s)
    sys.exit()


conf_file = 'conf.py'
pars = OptionParser()
pars.add_option('-c', '--conf', default=conf_file,
        help='configuration file, default: ' + conf_file)
pars.add_option('', '--show-buckets', action='store_true')
pars.add_option('', '--show-keys', action='store_true')
pars.add_option('-b', '--bucket')
pars.add_option('-k', '--key')
pars.add_option('-f', '--file', help='filename as key if --key option is empty, need --bucket option')
pars.add_option('', '--prefix-key', help='prefix key, need --file option')
pars.add_option('-t', '--content-type', help='example: image/jpeg')
pars.add_option('-a', '--acl', help='choices: public-read, private')
pars.add_option('', '--delete', action='store_true', help='delete empty bucket or key')
option, remain = pars.parse_args(sys.argv[1:])

conf_file = option.conf
conf = imp.load_source('conf', conf_file)

conn = boto.connect_s3(
        aws_access_key_id = conf.access_key,
        aws_secret_access_key = conf.secret_key,
        host = conf.host,
        is_secure = True,
        calling_format = boto.s3.connection.OrdinaryCallingFormat(),
)

if option.show_buckets:
    for bucket in conn.get_all_buckets():
        print('Bucket {b}'.format(b=bucket.name))

elif option.show_keys:
    if option.bucket:
        buckets = [conn.lookup(option.bucket)]
    else:
        buckets = conn.get_all_buckets()
    for bucket in buckets: 
        for key in bucket.list():
            print('Bucket {b}, key {k}'.format(b=bucket.name, k=key.name))

elif option.file:
    filename = option.file
    if not os.path.exists(filename):
        error('File {f} does not exists.'.format(filename=filename))
    f = open(filename)
    data = f.read()
    f.close()
    if option.key:
        key_name = option.key
    else:
        key_name = os.path.split(filename)[-1]
        if option.prefix_key:
            key_name = option.prefix_key + key_name
    bucket = get_bucket(True)
    key = bucket.lookup(key_name)
    if key:
        print('Key {k} already exists, size {b} bytes'.format(k=key_name,
            b=key.size))
    else:
        print('Create key {k}'.format(k=key_name))
        key = bucket.new_key(key_name)
        if option.content_type:
            header = {'Content-Type': option.content_type}
            key.set_contents_from_string(data, header)
        else:
            key.set_contents_from_string(data)
    if option.acl:
        key.set_canned_acl(option.acl)

elif option.acl:
    bucket = get_bucket()
    key = get_key()
    if option.acl:
        key.set_canned_acl(option.acl)

elif option.delete:
    bucket = get_bucket()
    if option.key:
        key = get_key()
        print('Delete key {k}'.format(k=key.name))
        key.delete()
    else:
        if bucket.get_all_keys(maxkeys=1):
            print('Can not delete bucket {b} because it is not empty.'.format(
                b=bucket.name))
        else:
            print('Delete bucket {b}'.format(b=bucket.name))
            bucket.delete()
